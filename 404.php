<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package timberscores
 */

$context = timberscores_get_context();

timberscores_render_page('assets/views/404.twig', $context);
